extends Node


signal inventory_slot_update(position: int, slot_info: Dictionary)
signal inventory_mouse_update(slot_info: Dictionary)
signal inventory_max_slots_update(max_slots: int)


var player: Player
var world: Node3D
var target_creature: Creature:
	get = get_target_creature


func _ready():
	randomize()


func add_player(new_player: Player) -> void:
	player = new_player
	player.inventory.drop.connect(drop_stack)
	player.inventory.modified_slot.connect(update_inventory_slot)
	player.inventory.modified_mouse.connect(update_inventory_mouse)
	player.inventory.modified_slot_space.connect(func(max_slots: int): inventory_max_slots_update.emit(max_slots))


func end_level() -> void:
	player.queue_free()


func get_inventory_max_slots() -> int:
	if player == null:
		return 0
	return player.inventory.max_slots


func interact_with_inventory_slot(position: int) -> void:
	if player == null:
		return
	var inventory = player.inventory
	if inventory.on_mouse:
		inventory.add_from_mouse(position)
	else:
		inventory.remove_to_mouse(position)


func drop_from_inventory_mouse() -> void:
	if player == null:
		return
	player.inventory.drop_from_mouse()


func delete_from_inventory_mouse() -> void:
	if player == null:
		return
	player.inventory.delete_from_mouse()


func update_inventory_slot(position: int) -> void:
	inventory_slot_update.emit(position, player.inventory.get_info_from_position(position))


func update_inventory_mouse() -> void:
	inventory_mouse_update.emit(player.inventory.get_info_from_mouse())


func toggle_inventory(is_open: bool) -> void:
	if player == null:
		return
	if is_open:
		player.inventory.open_inventory()
	else:
		player.inventory.close_inventory()
	player.set_physics_process(!is_open)


func drop_stack(stack: InventorySlot) -> void:
	pass #TODO: add interaction with world


func get_target_creature() -> Creature:
	return target_creature


func ritual_failed() -> void:
	pass #TODO: end run (failure)


func ritual_summoned(creature: Creature):
	if creature == target_creature:
		pass #TODO: end run (success)
	elif creature.killer:
		pass #TODO: end run (death)
	else:
		pass #TODO: end run (failure)
