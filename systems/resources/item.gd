class_name Item
extends Resource


@export_group('Item')
@export var name: String
@export_multiline var description: String
@export var texture: Texture2D
@export_range(1,99) var stack_limit: int = 1

@export_subgroup('Item Properties')
@export var type: ItemTypes
@export var properties: Array[ItemProperties]

enum ItemTypes {
	FOOD,
	ANIMAL,
	KNOWLEDGE,
	MAGICAL,
	NATURAL,
	RESOURCE,
}
enum ItemProperties {
	LIGHT,
	DARK,
	TRANSPARENT,
	HEAVY,
	BIG,
	SMALL,
	CLEAN,
	DIRTY,
	PERFUME,
	TASTY,
	EDIBLE,
	POISONOUS,
	LIQUID,
	OLD,
	NEW,
}


static func get_empty_info() -> Dictionary:
	return {
		'name': '',
		'description': '',
		'texture': null,
		'stack_limit': 0,
		'type': [],
		'properties': [],
	}


func get_info() -> Dictionary:
	return {
		'name': name,
		'description': description,
		'texture': texture,
		'stack_limit': stack_limit,
		'type': ItemTypes.keys()[type].capitalize(),
		'properties': _get_properties_as_strings(),
	}


func _get_properties_as_strings() -> Array[String]:
	var strs: Array[String] = []
	for property in properties:
		strs.append(ItemProperties.keys()[property].capitalize())
	return strs
