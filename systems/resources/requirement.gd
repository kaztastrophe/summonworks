class_name Requirement
extends Resource


var classification: Classifications = Classifications.ITEM
var requirement:
	set = _set_requirement


enum Classifications {
	ITEM,
	ITEM_TYPE,
	ITEM_PROPERTY,
}


func _set_requirement(value) -> void:
	var error = 'Requirement should be a valid '
	if classification == Classifications.ITEM:
		error += 'item.'
		assert(value is Item, error)
	elif classification == Classifications.ITEM_TYPE:
		error += 'item type.'
		assert(value >= 0, error)
		assert(value < Item.ItemTypes.size(), error)
	elif classification == Classifications.ITEM_PROPERTY:
		error += 'item property.'
		assert(value >= 0, error)
		assert(value < Item.ItemProperties.size(), error)
	else:
		error = 'Requirement classification not set.'
		assert(false, error)
	requirement = value


func fulfills_requirement(item: Item) -> bool:
	if classification == Classifications.ITEM:
		return _fulfills_item(item)
	elif classification == Classifications.ITEM_TYPE:
		return _fulfills_type(item)
	elif classification == Classifications.ITEM_PROPERTY:
		return _fulfills_property(item)
	else:
		return false


func _fulfills_item(item: Item) -> bool:
	if requirement is Item:
		return requirement == item
	return false


func _fulfills_type(item: Item) -> bool:
	if classification == Classifications.ITEM_TYPE and requirement is int:
		return requirement == item.type
	return false


func _fulfills_property(item: Item) -> bool:
	if classification == Classifications.ITEM_PROPERTY and requirement is int:
		return item.properties.has(requirement)
	return false
