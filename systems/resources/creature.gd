class_name Creature
extends Resource


@export_group('Creature')
@export var name: String
@export_multiline var description: String
@export var killer: bool = false
@export_range(1,5) var star: int:
	set(value):
		if value < 1:
			value = 1
		elif value > 5:
			value = 5
		star = value

@export_subgroup('Ritual')
@export var requirements: Array[Requirement]
@export_multiline var hint: String


func fulfills_requirements(items: Array[Item]):
	var is_wrong
	for requirement in requirements:
		is_wrong = true
		for item in items:
			if requirement.fulfills_requirement(item):
				is_wrong = false
				break
		if is_wrong:
			return false
	return true
