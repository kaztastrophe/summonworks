class_name InteractibleBase
extends Resource


@export_group('Interactible')
@export var name: String
@export var mesh: Mesh
@export var rarity: Rarities
@export var consume_on_empty: bool = false
@export_range(0,200) var time_to_fill: int = 0

@export_subgroup('Item')
@export var item_possibilities: Array[Item]
@export_range(1,10) var max_item_amount: int


enum Rarities {
	PLENTIFUL,
	COMMOM,
	UNCOMMOM,
	RARE,
	UNIQUE,
}
