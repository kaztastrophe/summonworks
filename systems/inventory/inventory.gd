class_name Inventory
extends Resource


signal drop(stack: InventorySlot)
signal modified_slot(position: int)
signal modified_mouse()
signal modified_slot_space(slots: int)


@export_group('Inventory')
@export var max_slots: int = 6 :
	set = set_max_slots
@export var slots: Array[InventorySlot] = []

var on_mouse: InventorySlot:
	set(value):
		on_mouse = value
		modified_mouse.emit()
var is_open: bool = false


func _ready():
	slots.resize(max_slots)


func set_max_slots(value: int):
	max_slots = value
	_update_slots_size()


func add_item(item: Item) -> void:
	add_stack(InventorySlot.new(item))


func add_stack(stack: InventorySlot) -> void:
	var positions = _get_slots_with_item(stack.item)
	for position in positions:
		var added = _add_to_position(position, stack)
		if added:
			return
	add_new_stack(stack)


func add_new_stack(stack: InventorySlot) -> void:
	var positions = _get_empty_slots()
	if positions.is_empty():
		if is_open and on_mouse == null:
			on_mouse = stack
		else:
			drop.emit(stack)
	else:
		_add_to_position(positions[0], stack)


func add_from_mouse(position: int) -> bool:
	if !on_mouse:
		print('Mouse empty')
		return false
	print('Adding %s to slot %d from mouse' % [on_mouse, position])
	var added = _add_to_position(position, on_mouse)
	if added:
		on_mouse = null
	return added


func remove_stack(stack: InventorySlot) -> InventorySlot:
	var position = slots.find(stack)
	if position == -1:
		return null
	var slot = slots[position]
	_empty_position(position)
	return slot


func remove_from_position(position: int, amount: int = -1) -> InventorySlot:
	var slot = get_from_position(position)
	if slot == null:
		return null
	return slot.remove_amount(amount)


func remove_to_mouse(position: int, amount: int = -1) -> bool:
	if on_mouse:
		print('Mouse already in use')
		return false
	on_mouse = remove_from_position(position, amount)
	print('Removing %s from slot %d to mouse' % [on_mouse, position])
	return true if on_mouse else false


func drop_from_position(position: int) -> void:
	var stack = get_from_position(position)
	if stack == null:
		return
	drop.emit(stack)
	_empty_position(position)


func drop_from_mouse() -> void:
	if on_mouse:
		drop.emit(on_mouse)
		on_mouse = null


func delete_from_mouse() -> void:
	on_mouse = null


func get_from_position(position: int) -> InventorySlot:
	if _is_valid_position(position):
		return slots[position]
	return null


func get_info_from_position(position: int) -> Dictionary:
	if _is_valid_position(position) and slots[position] is InventorySlot:
		return slots[position].get_info()
	return InventorySlot.get_empty_info()


func get_info_from_mouse() -> Dictionary:
	if on_mouse:
		return on_mouse.get_info()
	return InventorySlot.get_empty_info()


func open_inventory() -> void:
	is_open = true


func close_inventory() -> void:
	if on_mouse != null:
		drop.emit(on_mouse)
		on_mouse = null
	is_open = false


func _add_to_position(position: int, stack: InventorySlot) -> bool:
	if !_is_valid_position(position):
		return false
	
	if slots[position] == null:
		slots[position] = stack
		stack.stack_empty.connect(remove_stack)
		stack.stack_full.connect(add_stack)
	else:
		var added = slots[position].add_stack(stack)
		if !added:
			return false
	modified_slot.emit(position)
	return true


func _empty_position(position: int) -> void:
	if _is_valid_position(position):
		slots[position] = null
		modified_slot.emit(position)


func _get_empty_slots() -> Array[int]:
	var positions: Array[int] = []
	for position in slots.size():
		if slots[position] == null:
			positions.append(position)
	return positions


func _get_slots_with_item(item: Item) -> Array[int]:
	var positions: Array[int] = []
	for position in slots.size():
		if slots[position] is InventorySlot and slots[position].is_item(item):
			positions.append(position)
	return positions


func _is_valid_position(position: int) -> bool:
	return position > 0 and position < slots.size()


func _update_slots_size() -> void:
	if max_slots > slots.size():
		slots.resize(max_slots)
	elif max_slots < slots.size():
		var surplus = slots.slice(max_slots)
		slots.resize(max_slots)
		for stack in surplus:
			if stack != null:
				add_stack(stack)
	modified_slot_space.emit(max_slots)
