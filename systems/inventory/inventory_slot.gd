class_name InventorySlot
extends Resource


signal stack_empty(slot: InventorySlot)
signal stack_full(overflow: InventorySlot)


var item: Item
var amount: int = 1:
	set = _set_amount


func _init(p_item: Item, p_amount: int = 1):
	item = p_item
	
	var amount_error = 'Invalid amount for new InventorySlot (%d).' % p_amount
	assert(p_amount > 0, amount_error)
	assert(p_amount <= p_item.stack_limit, amount_error)
	_set_amount(p_amount)


func _to_string():
	if item:
		return '%s (%d)' % [item.name, amount]


func _set_amount(value: int) -> void:
	if value <= 0:
		amount = 0
		_empty_stack()
	elif amount > 0:
		var overflow = _calc_overflow(value)
		amount = value - overflow
		_overflow(overflow)


static func get_empty_info() -> Dictionary:
	var dict = Item.get_empty_info()
	dict['amount'] = 0
	return dict


func get_info() -> Dictionary:
	var dict = item.get_info()
	dict['amount'] = amount
	return dict


func add_item(to_add: Item) -> bool:
	if item != to_add:
		return false
	return _add_amount(1)


func add_stack(stack: InventorySlot) -> bool:
	if stack.item != item:
		return false
	return _add_amount(stack.amount)


func _add_amount(to_add: int) -> bool:
	var overflow = _calc_overflow(to_add)
	if overflow == to_add:
		return false
	else:
		amount += to_add
		return true


func _calc_overflow(to_add: int) -> int:
	return max(to_add + amount - item.stack_limit, 0)


func remove_amount(to_remove: int) -> InventorySlot:
	if to_remove == -1:
		to_remove = amount
	else:
		to_remove = min(amount, to_remove)
	
	if to_remove <= 0:
		return null
	amount -= to_remove
	return InventorySlot.new(item, to_remove)


func remove_half() -> InventorySlot:
	var to_remove = floor(amount/2)
	return remove_amount(to_remove)


func is_full() -> bool:
	return amount >= item.stack_limit


func is_item(compare: Item) -> bool:
	return compare == item


func _empty_stack() -> void:
	stack_empty.emit(self)


func _overflow(overflow_amount: int) -> void:
	if overflow_amount <= 0:
		return
	
	stack_full.emit(InventorySlot.new(item, min(overflow_amount, item.stack_limit)))
	_overflow(overflow_amount - item.stack_limit)
