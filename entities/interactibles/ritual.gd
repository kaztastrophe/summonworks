class_name Ritual
extends StaticBody3D


signal ritual_failed
signal summoned(creature: Creature)


@export var possible_creatures: Array[Creature] = []

var active_items: Array[Item] = []


const MAX_ITEMS = 6


func _ready():
	ritual_failed.connect(Bridge.ritual_failed)
	summoned.connect(Bridge.ritual_summoned)


func add_item(item: Item) -> bool:
	if active_items.size() < MAX_ITEMS:
		active_items.append(item)
		return true
	return false


func perform_ritual() -> void:
	if Bridge.get_target_creature().fulfills_requirements(active_items):
		summon(Bridge.get_target_creature())
		return
	
	possible_creatures.shuffle()
	for creature in possible_creatures:
		if creature.fulfills_requirements(active_items):
			summon(creature)
			return
	#TODO: add fail (explosion?) animation
	ritual_failed.emit()


func summon(creature: Creature) -> void:
	#TODO: add summon animation
	summoned.emit(creature)
