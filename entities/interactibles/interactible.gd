@tool
class_name Interactible
extends StaticBody3D


@export var base: InteractibleBase:
	set = _set_base
@export_range(0,50) var current_amount: int = 1:
	set = _set_current_amount
var item: Item


func _ready():
	collision_layer = 0b110
	if (not Engine.is_editor_hint()) and base == null:
		queue_free()
	
	update_configuration_warnings()
	_add_body()


func _get_configuration_warnings():
	var warnings = []
	
	if base == null:
		warnings.append('Please set an interactible base.')
	elif base.mesh == null:
		warnings.append('Please add a mesh to the interactible base.')
	
	return warnings


func randomize_current_amount() -> void:
	current_amount = randi()


func interact() -> Item:
	if current_amount <= 0:
		return null
	current_amount -= 1
		
	if current_amount == 0 and base.consume_on_empty:
		destroy()
	elif base.time_to_fill > 0:
		auto_fill()
	
	return item


func auto_fill() -> void:
	if current_amount < base.max_item_amount:
		get_tree().create_timer(base.time_to_fill).timeout.connect(_add_item)


func destroy() -> void:
	queue_free()


func _set_current_amount(value: int) -> void:
	if value < 0:
		current_amount = 0
	elif base == null:
		current_amount = 1
	elif value > base.max_item_amount:
		current_amount = base.max_item_amount
	else:
		current_amount = value
	update_configuration_warnings()


func _set_base(new: InteractibleBase) -> void:
	base = new
	item = base.item_possibilities.pick_random()
	_add_body()
	update_configuration_warnings()


func _add_body() -> void:
	if base == null:
		return
	
	var mesh = get_node_or_null('Mesh')
	if mesh:
		mesh.queue_free()
	mesh = MeshInstance3D.new()
	mesh.mesh = base.mesh
	add_child(mesh)
	mesh.name = 'Mesh'
	
	var collision = get_node_or_null('Collision')
	if collision:
		collision.queue_free()
	collision = CollisionShape3D.new()
	collision.shape = base.mesh.create_convex_shape()
	add_child(collision)
	collision.name = 'Collision'
	
	if Engine.is_editor_hint():
		mesh.owner = get_tree().edited_scene_root
		collision.owner = get_tree().edited_scene_root


func _add_item() -> void:
	if current_amount < base.max_item_amount:
		current_amount += 1
