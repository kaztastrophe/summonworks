class_name Player
extends CharacterBody3D


const SPEED = 5.0
const JUMP_VELOCITY = 4.5

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")

@export var inventory: Inventory


func _ready():
	Bridge.add_player(self)


func _physics_process(delta):
	# Add the gravity.
	if not is_on_floor():
		velocity.y -= gravity * delta

	# Handle jump.
	if Input.is_action_just_pressed("action_jump") and is_on_floor():
		velocity.y = JUMP_VELOCITY
	
	if Input.is_action_just_pressed("action_interact") and is_on_floor():
		_interact()

	# Get the input direction and handle the movement/deceleration.
	# As good practice, you should replace UI actions with custom gameplay actions.
	var input_dir = Input.get_vector("action_left", "action_right", "action_up", "action_down")
	var direction = (transform.basis * Vector3(input_dir.x, 0, input_dir.y)).normalized()
	if direction:
		velocity.x = direction.x * SPEED
		velocity.z = direction.z * SPEED
	else:
		velocity.x = move_toward(velocity.x, 0, SPEED)
		velocity.z = move_toward(velocity.z, 0, SPEED)
	
	var look_dir = Vector3(velocity.x,0,velocity.z)
	if !look_dir.is_equal_approx(Vector3.ZERO):
		$Mesh.look_at(position+look_dir)
	move_and_slide()


func _interact() -> void:
	print('Interact')
	var interactibles = %Detection.get_overlapping_bodies().filter(
		func(value):
			return value is Interactible
	)
	if interactibles.is_empty():
		return
	var item = interactibles.interact()
	print(item)
	inventory.add_item(item)
