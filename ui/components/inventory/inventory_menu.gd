extends PanelContainer


signal slot_clicked(slot_position: int)
signal drop_button_clicked()
signal delete_button_clicked()
signal toggle_inventory(is_open: bool)


@export_file('*.tscn') var slot_component_path
var current_slot_count: int = 0

const slot_component_name = 'Slot%d'


func _ready():
	Bridge.inventory_slot_update.connect(update_slot)
	Bridge.inventory_mouse_update.connect(update_mouse)
	Bridge.inventory_max_slots_update.connect(update_max_slots)
	slot_clicked.connect(Bridge.interact_with_inventory_slot)
	drop_button_clicked.connect(Bridge.drop_from_inventory_mouse)
	delete_button_clicked.connect(Bridge.delete_from_inventory_mouse)
	toggle_inventory.connect(Bridge.toggle_inventory)
	update_max_slots(Bridge.get_inventory_max_slots())


func update_max_slots(max_slots: int) -> void:
	var slot_difference = max_slots - current_slot_count
	if slot_difference > 0:
		for p in slot_difference:
			_add_slot()
	elif slot_difference < 0:
		for p in range(current_slot_count, max_slots):
			_remove_slot(p)


func update_slot(slot_position: int, slot_info: Dictionary) -> void:
	print('Update slot %d' % slot_position)
	var component = %Inventory.get_node(slot_component_name % slot_position)
	if component:
		component.update_all(
			slot_info.get('texture'),
			slot_info.get('amount', 1),
			slot_info.get('stack_limit', 1)
		)


func update_mouse(slot_info: Dictionary) -> void:
	Input.set_custom_mouse_cursor(slot_info.get('texture'))


func click_slot(slot_position: int) -> void:
	print('Interacting with slot %d' % slot_position)
	slot_clicked.emit(slot_position)


func click_delete_button() -> void:
	delete_button_clicked.emit()


func click_drop_button() -> void:
	drop_button_clicked.emit()


func toggle_visibility() -> void:
	visible = !visible
	toggle_inventory.emit(visible)


func _add_slot() -> void:
	var component = load(slot_component_path).instantiate()
	component.slot_position = current_slot_count
	component.clicked.connect(click_slot)
	%Inventory.add_child(component)
	component.name = slot_component_name % current_slot_count
	current_slot_count += 1


func _remove_slot(slot_position: int) -> void:
	var component = %Inventory.get_node(slot_component_name % slot_position)
	if component:
		%Inventory.remove_child(component)
		component.queue_free()
		current_slot_count -= 1
