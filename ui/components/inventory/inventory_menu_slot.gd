extends PanelContainer


signal clicked(slot_position: int)


var slot_position: int = 0:
	set(value):
		assert(value >= 0, 'Slot position can\'t be negative.')
		slot_position = max(0,value)


func _gui_input(event):
	if event is InputEventMouseButton:
		if event.button_index == MOUSE_BUTTON_LEFT and event.pressed:
			print('Slot %d was clicked.' % slot_position)
			clicked.emit(slot_position)


func update_all(textue: Texture2D, amount: int, max_amount: int) -> void:
	%Icon.texture = textue
	%Amount.value = amount
	%Amount.max_value = max_amount


func update_texture(texture: Texture2D) -> void:
	%Icon.texture = texture


func update_current_amount(amount: int) -> void:
	%Amount.value = amount


func update_max_amount(max_amount: int) -> void:
	%Amount.max_value = max_amount
